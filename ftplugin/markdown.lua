if require("zk.util").notebook_root(vim.fn.expand("%:p")) ~= nil then
  local function map(...)
    vim.api.nvim_buf_set_keymap(0, ...)
  end
  local opts = { noremap = true, silent = false }
  local direct_command_to_call_this_from_callback =
    [[:'<,'>lua vim.ui.input('Title: ', function(title) vim.cmd("'<,'>ZkNewFromContentSelection {title = '" .. title .. "'}") end) <CR> ]]

  map("v", "<leader>znc", direct_command_to_call_this_from_callback, opts)
  map("v", "<leader>znt", ":'<,'>ZkNewFromTitleSelection<CR>", opts)

  -- Open notes linking to the current buffer.
  map("n", "<leader>zb", "<Cmd>ZkBacklinks<CR>", opts)
  -- Alternative for backlinks using pure LSP and showing the source context.
  --map('n', '<leader>zb', '<Cmd>lua vim.lsp.buf.references()<CR>', opts)
  -- Open notes linked by the current buffer.
  map("n", "<leader>zl", "<Cmd>ZkLinks<CR>", opts)

  -- Preview a linked note.
  map("n", "K", "<Cmd>lua vim.lsp.buf.hover()<CR>", opts)
  -- Open the code actions for a visual selection.
  map("v", "<leader>za", ":'<,'>lua vim.lsp.buf.range_code_action()<CR>", opts)
end


vim.keymap.set("n", "<leader>tc", ":setlocal <C-R>=&conceallevel ? 'conceallevel=0' : 'conceallevel=2'<CR><CR>", { desc = "[T]oggle [C]onceallevel"} )
vim.keymap.set("n", "<leader>mp", ":MarkdownPreview<CR>", { desc = "Toggle [M]arkdown [P]review"} )
