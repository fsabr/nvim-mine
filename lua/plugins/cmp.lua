local luasnip_config = {
    "L3MON4D3/LuaSnip",
    build = (function()
        if vim.fn.has("win32") == 1 or vim.fn.executable("make") == 0 then
            return
        end
        return "make install_jsregexp"
    end)(),
}

local autopairs_config = {
    "windwp/nvim-autopairs",
    opts = {
        fast_wrap = {},
        disable_filetype = { "TelescopePrompt", "vim" },
    },
    config = function(_, opts)
        local npairs = require("nvim-autopairs")
        npairs.setup(opts)

        local Rule = require("nvim-autopairs.rule")
        -- npairs.add_rule(Rule("$$", "$$", {"tex", "markdown", "latex"}))
        npairs.add_rule(Rule("$", "$", {"tex", "markdown", "latex"}))

        -- setup cmp for autopairs
        local cmp_autopairs = require("nvim-autopairs.completion.cmp")
        require("cmp").event:on("confirm_done", cmp_autopairs.on_confirm_done())
    end,
}

-- For command line
-- This custom mappig setup causes CTRL-P, CTRL-N to fallback to history
-- browsing, unless user has explicitly typed something in the cmdline, then
-- these two activate to browse completion options.
local cmdline_cmp_state = "has_not_typed"
vim.api.nvim_create_autocmd({ "CmdlineEnter" }, {
    command = "lua cmdline_cmp_state = 'has_not_typed'",
})
vim.api.nvim_create_autocmd({ "CmdlineChanged" }, {
    callback = function()
        if cmdline_cmp_state == "has_not_typed" then
            cmdline_cmp_state = "has_typed"
        elseif cmdline_cmp_state == "has_browsed_history" then
            cmdline_cmp_state = "has_not_typed"
        end
    end,
})
local function select_or_fallback(cmp, select_action)
    return cmp.mapping(function(fallback)
        if cmdline_cmp_state == "has_typed" and cmp.visible() then
            select_action()
        else
            cmdline_cmp_state = "has_browsed_history"
            cmp.close()
            fallback()
        end
    end, { "i", "c" })
end

return {
    "hrsh7th/nvim-cmp",
    event = { "InsertEnter", "CmdLineEnter" },
    dependencies = {
        luasnip_config,
        autopairs_config,
        "saadparwaiz1/cmp_luasnip",
        "hrsh7th/cmp-nvim-lsp",
        "hrsh7th/cmp-nvim-lua",
        "hrsh7th/cmp-path",
        "hrsh7th/cmp-buffer",
        "hrsh7th/cmp-calc",
        "hrsh7th/cmp-cmdline",
        "rafamadriz/friendly-snippets",
        "onsails/lspkind.nvim",
    },
    config = function()
        local cmp = require("cmp")
        local lspkind = require("lspkind")
        local luasnip = require("luasnip")
        luasnip.config.setup({})
        require("luasnip.loaders.from_vscode").lazy_load()
        cmp.setup({
            snippet = {
                expand = function(args)
                    luasnip.lsp_expand(args.body)
                end,
            },
            mapping = cmp.mapping.preset.insert({
                ["<CR>"] = cmp.mapping.confirm({ select = true }),
                ["<C-Space>"] = cmp.mapping.complete({}),
                ["<C-l>"] = cmp.mapping(function()
                    if luasnip.expand_or_locally_jumpable() then
                        luasnip.expand_or_jump()
                    end
                end, { "i", "s" }),
                ["<C-h>"] = cmp.mapping(function()
                    if luasnip.locally_jumpable(-1) then
                        luasnip.jump(-1)
                    end
                end, { "i", "s" }),
            }),
            sources = {
                { name = "nvim_lua" },
                { name = "nvim_lsp" },
                { name = "luasnip" },
                { name = "path" },
                { name = "calc" },
                { name = "buffer",  keyword_length = 5 },
            },
            formatting = {
                format = lspkind.cmp_format({
                    mode = "symbol_text", -- show only symbol annotations
                    maxwidth = 31,        -- prevent the popup from showing more than provided characters (e.g 50 will not show more than 50 characters)
                    -- can also be a function to dynamically calculate max width such as
                    -- maxwidth = function() return math.floor(0.45 * vim.o.columns) end,
                    ellipsis_char = "…", -- when popup menu exceed maxwidth, the truncated part would show ellipsis_char instead (must define maxwidth first)
                    show_labelDetails = true, -- show labelDetails in menu. Disabled by default
                }),
            },
        })
        local cmdline_mapping = cmp.mapping.preset.cmdline({
            ["<C-n>"] = select_or_fallback(cmp, cmp.select_next_item),
            ["<C-p>"] = select_or_fallback(cmp, cmp.select_prev_item),
        })
        cmp.setup.cmdline('/', {
            mapping = cmdline_mapping,
            sources = {
                { name = 'buffer' }
            }
        })
        cmp.setup.cmdline(':', {
            mapping = cmdline_mapping,
            sources = cmp.config.sources({
                    { name = 'path' }
                },
                {
                    {
                        name = 'cmdline',
                        option = {
                            ignore_cmds = { 'Man', '!' }
                        }
                    }
                })
        })
    end,
}
