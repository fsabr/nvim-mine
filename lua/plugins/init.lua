local function install_lazy_if_not_present()
	--    See `:help lazy.nvim.txt` or https://github.com/folke/lazy.nvim for more info
	local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
	if not vim.loop.fs_stat(lazypath) then
		local lazyrepo = "https://github.com/folke/lazy.nvim.git"
		vim.fn.system({ "git", "clone", "--filter=blob:none", "--branch=stable", lazyrepo, lazypath })
	end ---@diagnostic disable-next-line: undefined-field
	vim.opt.rtp:prepend(lazypath)
end

install_lazy_if_not_present()

local lazy = require("lazy")

lazy.setup({
	require("plugins.catppuccin"),
	require("plugins.mason"),
	require("plugins.lspconfig"),
	require("plugins.conform"),
	require("plugins.treesitter"),
	require("plugins.telescope"),
	require("plugins.cmp"),
	require("plugins.comment"),
	require("plugins.gitsigns"),
	require("plugins.zk"),
	require("plugins.mdpreview"),
	{ "tpope/vim-fugitive", cmd = "G" },
	{ "tpope/vim-surround", event = "InsertEnter" },
	{
		"iamcco/markdown-preview.nvim",
		cmd = { "MarkdownPreviewToggle", "MarkdownPreview", "MarkdownPreviewStop" },
		ft = { "markdown" },
		build = function()
			vim.fn["mkdp#util#install"]()
		end,
	},
})
