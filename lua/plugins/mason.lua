return {
	"williamboman/mason.nvim",
	dependencies = { "nvim-telescope/telescope.nvim" },
	cmd = { "Mason", "MasonInstall", "MasonInstallAll", "MasonUpdate" },
	opts = {},
}
